package org.jungdo.upisbackendmock.repository;

import org.jungdo.upisbackendmock.entity.IndustrialPark;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IndustrialParkRepository extends JpaRepository<IndustrialPark, String> {
}
