package org.jungdo.upisbackendmock.repository;

import org.jungdo.upisbackendmock.entity.Home;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HomeRepository extends JpaRepository<Home, String> {
}
