package org.jungdo.upisbackendmock.controller;

import org.jungdo.upisbackendmock.service.map.common.CommonGeoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/api/geo")
public class GeoController {

    @Autowired
    private CommonGeoService geoService;

    @PostMapping(value = "/upload-shapefile", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<?> convertShapefileToGeoJSON(@RequestBody MultipartFile file,
                                                       @RequestParam(required = false) String crs) {
        if (crs == null) crs = "EPSG:3857";
        String geojson = geoService.convertZipShapefileToGeoJSON(file, crs);

        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(geojson);
    }
}
