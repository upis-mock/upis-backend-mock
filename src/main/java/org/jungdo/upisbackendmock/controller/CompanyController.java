package org.jungdo.upisbackendmock.controller;


import org.jungdo.upisbackendmock.payload.request.CompanyRequest;
import org.jungdo.upisbackendmock.payload.response.CompanyResponse;
import org.jungdo.upisbackendmock.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/company")
public class CompanyController {

    @Autowired
    private CompanyService companyService;

    @GetMapping
    public ResponseEntity<List<CompanyResponse>> list() {
        List<CompanyResponse> responses = companyService.list();
        return new ResponseEntity<>(responses, HttpStatus.OK);
    }

    @GetMapping("{id}")
    public ResponseEntity<CompanyResponse> getById(@PathVariable String id) {
        CompanyResponse response = companyService.getById(id);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    @PostMapping
    public ResponseEntity<CompanyResponse> create(@RequestBody CompanyRequest companyRequest) {
        CompanyResponse companyResponse = companyService.create(companyRequest);
        return new ResponseEntity<>(companyResponse, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<CompanyResponse> update(@PathVariable String id, @RequestBody CompanyRequest companyRequest) {
        CompanyResponse companyResponse = companyService.update(id, companyRequest);
        return new ResponseEntity<>(companyResponse, HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> delete(@PathVariable String id) {
        Boolean result = companyService.deleteById(id);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
