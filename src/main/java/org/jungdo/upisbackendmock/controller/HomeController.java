package org.jungdo.upisbackendmock.controller;

import org.jungdo.upisbackendmock.payload.request.HomeRequest;
import org.jungdo.upisbackendmock.payload.response.HomeResponse;
import org.jungdo.upisbackendmock.service.HomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/homes")
public class HomeController {

    @Autowired
    private HomeService homeService;

    @PostMapping
    public ResponseEntity<HomeResponse> createHome(@RequestBody HomeRequest homeRequest) {
        HomeResponse homeResponse = homeService.createHome(homeRequest);
        return new ResponseEntity<>(homeResponse, HttpStatus.CREATED);
    }
}
