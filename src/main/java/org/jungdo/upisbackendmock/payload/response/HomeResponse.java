package org.jungdo.upisbackendmock.payload.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HomeResponse {
    private String id;
    private String address;
    private double area;
}
