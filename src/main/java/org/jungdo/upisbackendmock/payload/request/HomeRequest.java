package org.jungdo.upisbackendmock.payload.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HomeRequest {
    private String address;
    private double area;
    private String wkt;
}
