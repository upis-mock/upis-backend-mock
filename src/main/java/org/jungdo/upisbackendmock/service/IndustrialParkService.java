package org.jungdo.upisbackendmock.service;

import org.jungdo.upisbackendmock.payload.request.IndustrialParkRequest;
import org.jungdo.upisbackendmock.payload.response.IndustrialParkResponse;

public interface IndustrialParkService {
    IndustrialParkResponse create(IndustrialParkRequest industrialParkRequest);
    IndustrialParkResponse update(String id, IndustrialParkRequest industrialParkRequest);
    boolean deleteById(String id);
}
