package org.jungdo.upisbackendmock.service.map.common;

import lombok.RequiredArgsConstructor;
import org.apache.coyote.BadRequestException;
import org.geotools.data.*;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.data.store.ReprojectingFeatureCollection;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.FeatureIterator;
import org.geotools.geojson.feature.FeatureJSON;
import org.geotools.geojson.geom.GeometryJSON;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.referencing.CRS;
import org.jungdo.upisbackendmock.entity.Home;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.io.WKTReader;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.filter.Filter;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.reactive.function.client.WebClient;

import java.io.*;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

@Service
@RequiredArgsConstructor
public class CommonGeoService {

    private String BASE_URL_MAPSTUDIO = "http://192.168.1.201:18884/mapstudio";
    private static final String DEST_DIR_UNZIP = "shapefile";

    private final WebClient webClient;

    public String apiWFS(String key, String body) {
        return  webClient.post()
                .uri("/wfs?KEY=" + key)
                .contentType(MediaType.APPLICATION_XML)
                .bodyValue(body)
                .retrieve()
                .bodyToMono(String.class)
                .block();
    }

    public  Geometry convertWKTToGeometry(String wkt) {
        try {
            GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory();
            WKTReader reader = new WKTReader(geometryFactory);

            return reader.read(wkt);
        }catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String convertZipShapefileToGeoJSON(MultipartFile zipFile, String crs) {
        File dir = new File(DEST_DIR_UNZIP);
        if (!dir.exists()) {
            dir.mkdirs();
        } else {
            deleteDirectory(dir);
            dir.delete();
            dir.mkdirs();
        }

        extractZipFile(zipFile);
        File shapefile = findShapefileComponents(DEST_DIR_UNZIP);
        String geoJson = convertShapefileToGeoJSON(shapefile, crs);

        deleteDirectory(new File(DEST_DIR_UNZIP));

        return geoJson;
    }

    private void extractZipFile(MultipartFile file) {
        try {
            File tempFile = File.createTempFile("shapefile", ".zip");
            file.transferTo(tempFile);
            String zipFilePath = tempFile.getAbsolutePath();

            FileInputStream fis;
            byte[] buffer = new byte[1024];

            fis = new FileInputStream(zipFilePath);
            ZipInputStream zis = new ZipInputStream(fis);
            ZipEntry ze = zis.getNextEntry();
            while (ze != null) {

                String fileName = ze.getName();
                String outputPath = DEST_DIR_UNZIP + File.separator + fileName;
                File newFile = new File(outputPath);
                //create directories for sub directories in zip
                new File(newFile.getParent()).mkdirs();
                FileOutputStream fos = new FileOutputStream(newFile);
                int len;
                while ((len = zis.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }
                fos.close();
                zis.closeEntry();
                ze = zis.getNextEntry();
            }
            zis.closeEntry();
            zis.close();
            fis.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static void deleteDirectory(File file) {
        for (File subfile : file.listFiles()) {
            if (subfile.isDirectory()) {
                deleteDirectory(subfile);
            }
            subfile.delete();
        }
    }

    private File findShapefileComponents(String dirPath) {
        File directory = new File(dirPath);
        File[] shapefiles = directory.listFiles((dir, name) -> name.endsWith(".shp"));

        if (shapefiles != null && shapefiles.length > 0) {
            String shapefilePrefix = StringUtils.stripFilenameExtension(shapefiles[0].getName());
            File[] relatedFiles = directory.listFiles((dir, name) ->
                    name.startsWith(shapefilePrefix)
                            && (name.endsWith(".shx") || name.endsWith(".prj")
                            || name.endsWith(".cpg") || name.endsWith(".dbf")));
            if (relatedFiles != null && relatedFiles.length == 4) {
                return shapefiles[0];
            }
        }
        return null;
    }

    private String convertShapefileToGeoJSON(File shapefile, String crs) {
        try {
            FileDataStore inputDataStore = FileDataStoreFinder.getDataStore(shapefile);
            String inputTypeName = inputDataStore.getTypeNames()[0];
            SimpleFeatureSource featureSource = inputDataStore.getFeatureSource(inputTypeName);
            CoordinateReferenceSystem targetCRS = CRS.decode(crs);
            SimpleFeatureCollection features = featureSource.getFeatures();

            try(SimpleFeatureIterator itr=features.features()){
                int count=0;
                while(itr.hasNext()&&count++<10) {
                    System.out.println(((Geometry) itr.next().getDefaultGeometry()).getCentroid());
                }
            }
            ReprojectingFeatureCollection rfc = new ReprojectingFeatureCollection(features, CRS.decode("epsg:3875"));
            try(SimpleFeatureIterator itr=rfc.features()){
                int count=0;
                while(itr.hasNext()&&count++<10) {
                    System.out.println(((Geometry) itr.next().getDefaultGeometry()).getCentroid());
                }
            }

            SimpleFeatureCollection transformedFeatures = new ReprojectingFeatureCollection(features, targetCRS);

            FeatureJSON featureJSON = new FeatureJSON(new GeometryJSON(15));
            StringWriter writer = new StringWriter();
            featureJSON.writeFeatureCollection(transformedFeatures, writer);

            return writer.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (FactoryException e) {
            e.printStackTrace();
        }

        return null;
    }
}
