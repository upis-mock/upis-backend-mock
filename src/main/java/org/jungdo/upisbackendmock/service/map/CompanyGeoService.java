package org.jungdo.upisbackendmock.service.map;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.jungdo.upisbackendmock.entity.Company;
import org.jungdo.upisbackendmock.service.map.common.CommonGeoService;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.Point;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class CompanyGeoService {

    @Value("${company.key-layer}")
    private String keyLayer;

    @Autowired
    private CommonGeoService commonGeoService;

    public String generateBodyCreate(Company company) {
        Geometry geometry = commonGeoService.convertWKTToGeometry(company.getWkt());
        if(!(geometry instanceof Point)) return null;

        return "<Transaction\n" +
                "    xmlns='http://www.opengis.net/wfs' service='WFS' version='1.1.0'\n" +
                "    xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:schemaLocation='http://www.opengis.net/wfs http://schemas.opengis.net/wfs/1.1.0/wfs.xsd'>\n" +
                "    <Insert idgen='GenerateNew' inputFormat='text/xml; subtype=gml/3.1.1' handle='STMT 1'\n" +
                "        xmlns:feature='http://www.opengis.net/wfs'>\n" +
                "        <SCP_COMPANY>\n" +
                "            <NAME>" + company.getName() + "</NAME>\n" +
                "            <ADDRESS>" + company.getAddress() + "</ADDRESS>\n" +
                "            <COMPANY_ID>" + company.getId() + "</COMPANY_ID>\n" +
                "            <SHAPE>\n" +
                "                <Point srsName='EPSG:3857'>\n" +
                "                    <pos>" + ((Point) geometry).getX() + " "
                                            + ((Point) geometry).getY() +
                "                    </pos>\n" +
                "                </Point>\n" +
                "            </SHAPE>\n" +
                "        </SCP_COMPANY>\n" +
                "    </Insert>\n" +
                "</Transaction>";
    }

    public String createFeature(Company company) {
        String body = generateBodyCreate(company);
        if(body != null) {
            return commonGeoService.apiWFS(keyLayer, body);
        }
        return null;
    }

    public String generateBodyUpdate(Company company) {
        Geometry geometry = commonGeoService.convertWKTToGeometry(company.getWkt());
        if(!(geometry instanceof Point)) return null;

        return "<Transaction\n" +
                "    xmlns='http://www.opengis.net/wfs' service='WFS' version='1.1.0'\n" +
                "    xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:schemaLocation='http://www.opengis.net/wfs http://schemas.opengis.net/wfs/1.1.0/wfs.xsd'>\n" +
                "    <Update typeName='SCP_COMPANY'\n" +
                "        xmlns:feature='http://www.opengis.net/wfs'>\n" +
                "        <Property>\n" +
                "            <Name>SHAPE</Name>\n" +
                "            <Value>\n" +
                "                <Point srsName='EPSG:3857'>\n" +
                "                    <pos>" + ((Point) geometry).getX() + " "
                                            + ((Point) geometry).getY() +
                "                    </pos>\n" +
                "                </Point>\n" +
                "            </Value>\n" +
                "        </Property>\n" +
                "        <Property>\n" +
                "            <Name>NAME</Name>\n" +
                "            <Value>" + company.getName() + "</Value>\n" +
                "        </Property>\n" +
                "        <Property>\n" +
                "            <Name>ADDRESS</Name>\n" +
                "            <Value>" + company.getAddress() + "</Value>\n" +
                "        </Property>\n" +
                "        <Filter xmlns='http://www.opengis.net/ogc'>\n" +
                "            <PropertyIsEqualTo>\n" +
                "               <PropertyName>COMPANY_ID</PropertyName>\n" +
                "               <Literal>" + company.getId() + "</Literal>\n" +
                "            </PropertyIsEqualTo>\n" +
                "        </Filter>\n" +
                "    </Update>\n" +
                "</Transaction>";
    }

    public String updateFeature(Company company) {
        String body = generateBodyUpdate(company);
        if(body != null) {
            return commonGeoService.apiWFS(keyLayer, body);
        }
        return null;
    }

    public String generateBodyDelete(String id) {

        return "<Transaction\n" +
                "    xmlns='http://www.opengis.net/wfs' service='WFS' version='1.1.0'\n" +
                "    xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:schemaLocation='http://www.opengis.net/wfs http://schemas.opengis.net/wfs/1.1.0/wfs.xsd'>\n" +
                "    <Delete typeName='SCP_COMPANY'\n" +
                "        xmlns:feature='http://www.opengis.net/wfs'>\n" +
                "        <Filter xmlns='http://www.opengis.net/ogc'>\n" +
                "            <PropertyIsEqualTo>\n" +
                "                <PropertyName>COMPANY_ID</PropertyName>\n" +
                "                <Literal>" + id + "</Literal>\n" +
                "            </PropertyIsEqualTo>\n" +
                "        </Filter>\n" +
                "    </Delete>\n" +
                "</Transaction>";
    }

    public String deleteFeature(String id) {
        String body = generateBodyDelete(id);
        return commonGeoService.apiWFS(keyLayer, body);
    }
}
