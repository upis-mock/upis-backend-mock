package org.jungdo.upisbackendmock.service.impl;

import lombok.AllArgsConstructor;
import org.jungdo.upisbackendmock.entity.Company;
import org.jungdo.upisbackendmock.payload.request.CompanyRequest;
import org.jungdo.upisbackendmock.payload.response.CompanyResponse;
import org.jungdo.upisbackendmock.repository.CompanyRepository;
import org.jungdo.upisbackendmock.service.CompanyService;
import org.jungdo.upisbackendmock.service.map.CompanyGeoService;
import org.jungdo.upisbackendmock.service.map.common.CommonGeoService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class CompanyServiceImpl implements CompanyService {

    private CompanyRepository companyRepository;
    private CompanyGeoService companyGeoService;

    @Override
    public List<CompanyResponse> list() {
        List<Company> companies = companyRepository.findAll();
        List<CompanyResponse> companyResponses = new ArrayList<>();
        for(Company company : companies) {
            CompanyResponse response = new CompanyResponse();
            response.setId(company.getId());
            response.setName(company.getName());
            response.setAddress(company.getAddress());
            response.setWkt(company.getWkt());
            companyResponses.add(response);
        }
        return companyResponses;
    }

    @Override
    public CompanyResponse getById(String id) {
        Optional<Company> company = companyRepository.findById(id);
        if(company.isPresent()) {
            CompanyResponse response = new CompanyResponse();
            response.setId(company.get().getId());
            response.setName(company.get().getName());
            response.setAddress(company.get().getAddress());
            response.setWkt(company.get().getWkt());

            return response;
        }
        return null;
    }

    @Override
    public CompanyResponse create(CompanyRequest companyRequest) {
        Company company = new Company();
        company.setName(companyRequest.getName());
        company.setAddress(companyRequest.getAddress());
        company.setWkt(companyRequest.getWkt());
        Company savedCompany = companyRepository.save(company);
        String result = companyGeoService.createFeature(savedCompany);

        if (result != null)
            return new CompanyResponse(String.valueOf(savedCompany.getId()), savedCompany.getName(), savedCompany.getAddress(), savedCompany.getWkt());

        return null;
    }

    @Override
    public CompanyResponse update(String id, CompanyRequest companyRequest) {
        Company company = companyRepository.findById(id).orElse(null);
        if (company != null) {
            company.setName(companyRequest.getName());
            company.setAddress(companyRequest.getAddress());
            company.setWkt(companyRequest.getWkt());
            Company savedCompany = companyRepository.save(company);
            String result = companyGeoService.updateFeature(savedCompany);

            if (result != null)
                return new CompanyResponse(String.valueOf(savedCompany.getId()), savedCompany.getName(), savedCompany.getAddress(), savedCompany.getWkt());
        }

        return null;
    }

    @Override
    public boolean deleteById(String id) {
        Company company = companyRepository.findById(id).orElse(null);
        if (company != null) {
            companyRepository.deleteById(id);
            String result = companyGeoService.deleteFeature(id);
            return  true;
        }
        return false;
    }
}
