package org.jungdo.upisbackendmock.service.impl;

import lombok.AllArgsConstructor;
import org.jungdo.upisbackendmock.entity.Home;
import org.jungdo.upisbackendmock.payload.request.HomeRequest;
import org.jungdo.upisbackendmock.payload.response.HomeResponse;
import org.jungdo.upisbackendmock.repository.HomeRepository;
import org.jungdo.upisbackendmock.service.map.common.CommonGeoService;
import org.jungdo.upisbackendmock.service.HomeService;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class HomeServiceImpl implements HomeService {

    private HomeRepository homeRepository;
    private CommonGeoService geoService;

    @Override
    public HomeResponse createHome(HomeRequest homeRequest) {
        Home home = new Home();
        home.setAddress(homeRequest.getAddress());
        home.setArea(homeRequest.getArea());
        Home savedHome = homeRepository.save(home);

        return new HomeResponse(savedHome.getId().toString(), savedHome.getAddress(), savedHome.getArea());
    }
}
