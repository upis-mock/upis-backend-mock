package org.jungdo.upisbackendmock.service;

import org.jungdo.upisbackendmock.payload.request.CompanyRequest;
import org.jungdo.upisbackendmock.payload.response.CompanyResponse;

import java.util.List;

public interface CompanyService {

    List<CompanyResponse> list();
    CompanyResponse getById(String id);
    CompanyResponse create(CompanyRequest companyRequest);
    CompanyResponse update(String id, CompanyRequest companyRequest);
    boolean deleteById(String id);
}
