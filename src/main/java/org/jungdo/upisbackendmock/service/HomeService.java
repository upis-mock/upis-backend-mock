package org.jungdo.upisbackendmock.service;

import org.jungdo.upisbackendmock.payload.request.HomeRequest;
import org.jungdo.upisbackendmock.payload.response.HomeResponse;

public interface HomeService {
    HomeResponse createHome(HomeRequest homeRequest);
}
