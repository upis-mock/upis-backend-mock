package org.jungdo.upisbackendmock.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@Entity
@Table(name = "INDUSTRIAL_PARK")
@NoArgsConstructor
@AllArgsConstructor
public class IndustrialPark {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;

    private String name;

    private String address;

    private String wkt;

}
