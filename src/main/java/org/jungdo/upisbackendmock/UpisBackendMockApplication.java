package org.jungdo.upisbackendmock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UpisBackendMockApplication {

	public static void main(String[] args) {
		SpringApplication.run(UpisBackendMockApplication.class, args);
	}

}
